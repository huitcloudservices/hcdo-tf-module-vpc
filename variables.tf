variable "vpc_cidr" {
  type = "string"
  description = "CIDR Notated IP address network for VPC"
}

data "aws_availability_zones" "available" {}

variable "group_name" {
  type = "string"
  description = "Group name for VPC (e.g. admints)"
}
variable "environment" {
  type = "string"
  description = "Environment for the application, one of: dev, test, stage, prod"
}
variable "environment_map" {
  type    = "map"
  description = "Provides lookup from environment to known strings, does not need to be passed"
  default = {
    dev   = "Development"
    test  = "Testing"
    stage = "Staging"
    prod  = "Production"
  }
}
variable "account_type" {
  type = "string"
  description = "Account Type, one of: standard or level4. Level4 only for dedicated level4 accounts (e.g. peoplesoft)"
  default = "standard"
}
variable "dns_name_servers" {
  type = "list"
  description = "List of DNS servers to use, defaults to Harvard Infoblox failing back to Amazon local DNS"
  default = ["128.103.1.1", "128.103.200.101", "128.103.201.100", "AmazonProvidedDNS"]
}
variable "ntp_servers" {
  type = "list"
  description = "List of NTP servers to use, defaults to Harvard time servers"
  default = ["128.103.1.6", "128.103.1.35", "128.103.38.165"]
}
variable "aws_region" {
  type = "string"
  description = "Region string which the VPC resides in, defaults to us-east-1"
  default = "us-east-1"
}
# Subnets for VPC Creation
variable "elb_standard_subnets"{
  type = "list"
  description = "List of subnets for standard ELB's in CIDR notation"
}
variable "elb_level4_subnets"{
  type = "list"
  description = "List of subnets for Level4 ELB's in CIDR notation"
}
variable "app_standard_subnets"{
  type = "list"
  description = "List of subnets for Standard Apps in CIDR notation"
}
variable "app_level4_subnets"{
  type = "list"
  description = "List of subnets for Level4 Apps in CIDR notation"
}
variable "db_standard_subnets"{
  type = "list"
  description = "List of subnets for Standard DB's in CIDR notation"
}
variable "db_level4_subnets"{
  type = "list"
  description = "List of subnets for Level4 DB's in CIDR notation"
}
variable "number_of_azs" {
  type = "string"
  default = "3"
  description = "Number of AZ's to use, always 3"
}