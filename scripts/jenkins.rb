#!/home/ec2-user/.rvm/wrappers/ruby-2.3.0@cfndsl/ruby
# START OF SUBNETTING
require 'ipaddress'
require 'net/http'
require 'json'

atlas_name=ENV["Atlas_Environment_Name"]
atlas_org='huit-cloud-dev'
atlas_api_token=''

#These are not used
elb_public_subnets = []
nat_public_subnets = []

elb_private_standard_subnets = []
elb_private_level4_subnets = []
app_private_standard_subnets = []
app_private_level4_subnets = []
db_private_level4_subnets = []
db_private_standard_subnets = []

#VpcCidr = "100.64.0.0/20" # FOR TESTING ONLY
#vpc_super_network = IPAddress(VpcCidr)
vpc_super_network = IPAddress(ENV["Vpc_Cidr"])
vpc_subnetting_networks = vpc_super_network.split(4) # 4x /22

# Start of Network Segment 1

# first /22 (remainder 3 x /22)
network_seg1 = vpc_subnetting_networks.shift
  # 2x /23 from /22 into an array
  seg1_sub1 = network_seg1.subnet(23)
  #Take first element and make 4x /25
  seg1_sub1_25 = seg1_sub1.shift.subnet(25)

    #Take the first 3 /25's and make the public ELB networks
    seg1_sub1_25.shift(3).each { |net|
      elb_public_subnets << net
    }

    # Take the last /25 and convert to /28 for the NAT's
    seg1_sub1_25.shift.subnet(26).shift.
      subnet(28).shift(3).each { |net|
        nat_public_subnets << net
    }

    # Take the second element of the /23 and convert to /25
    seg1_sub2_25 = seg1_sub1.shift.subnet(25)

      # Take the first 3 /25's and make the private ELB subnets
      seg1_sub2_25.shift(3).each { |net|
          elb_private_standard_subnets << net
      }

      # Take the last /25 and use it for elb private level4
      elb_private_level4_subnets << seg1_sub2_25.shift

# End of Network Segment 1

# Start of Network Segment 2

# Take the second /22 off the master array
network_seg2 = vpc_subnetting_networks.shift

  # Split the /22 into two /23's
  seg2_sub1 = network_seg2.subnet(23)

    # Take the first /23 and make it into two /24's
    seg2_sub1_24 = seg2_sub1.shift.subnet(24)

      # Take the first /24 and make two /25's for the last two elb private
      # level 4 subnets
      seg2_sub1_24.shift.subnet(25).each { |net|
        elb_private_level4_subnets << net
      }

      # Take the second /24 and make it the first private app subnet
      app_private_standard_subnets << seg2_sub1_24.shift

      # Take the second /23 and split it into two /24's for the last two
      # standard app networks
      seg2_sub1.shift.subnet(24).each { |net|
        app_private_standard_subnets << net
      }

# End of Network Segment 2

# Start of Network Segment 3

# Take the third /22 off the master array
network_seg3 = vpc_subnetting_networks.shift

# Split into four /24's
seg3_sub1 = network_seg3.subnet(24)

  # Take the first three /24's and make the private app level4 subnets
  seg3_sub1.shift(3).each { |net|
    app_private_level4_subnets << net
  }

  # Take the fourth /24 and split it into four /26's
  seg3_sub1_26 = seg3_sub1.shift.subnet(26)

  # Take the first three /26's for the db standard private networks
  seg3_sub1_26.shift(3).each { |net|
      # 1-3 /26 = db1/2/3
      db_private_standard_subnets << net
  }

  # Take the last /26 and make the first db level4 subnet
  db_private_level4_subnets << seg3_sub1_26.shift

# End of Network Segment 3

# Start of Network Segment 4

# Take the fourth and last /22 off the master array and split it immediately
# into two /23's
network_seg3 = vpc_subnetting_networks.shift
seg3_sub1 = network_seg3.subnet(23)

# Take the first /24 off of the array, the remainer of the subnet is
# unutilized
seg3_sub1.shift.subnet(24).shift.
  # Convert the /24 into two /25's for each of reading
  subnet(25).shift.
    # Then convert the /25 into two /26's for the final two db level4 nets
    # Again doing this for ease of reading
    subnet(26).each { |net|
      db_private_level4_subnets << net
}

elb_standard_subnets = []
elb_private_standard_subnets.each do |i|
  elb_standard_subnets << "#{i.address}/#{i.prefix}"
end
elb_level4_subnets = []
elb_private_level4_subnets.each do |i|
  elb_level4_subnets << "#{i.address}/#{i.prefix}"
end
app_standard_subnets = []
app_private_standard_subnets.each do |i|
  app_standard_subnets << "#{i.address}/#{i.prefix}"
end
app_level4_subnets = []
app_private_level4_subnets.each do |i|
  app_level4_subnets << "#{i.address}/#{i.prefix}"
end
db_standard_subnets = []
db_private_standard_subnets.each do |i|
  db_standard_subnets << "#{i.address}/#{i.prefix}"
end
db_level4_subnets = []
db_private_level4_subnets.each do |i|
  db_level4_subnets << "#{i.address}/#{i.prefix}"
end

atlas_subnet_vars = {
  variables: {
    elb_standard_subnets: elb_standard_subnets.join(','),
    elb_level4_subnets: elb_level4_subnets.join(','),
    app_standard_subnets: app_standard_subnets.join(','),
    app_level4_subnets: app_level4_subnets.join(','),
    db_standard_subnets: db_standard_subnets.join(','),
    db_level4_subnets: db_level4_subnets.join(',')
  }
}


uri = URI.parse("https://atlas.hashicorp.com/api/v1/environments/#{atlas_org}/#{atlas_name}/variables")
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
request = Net::HTTP::Put.new(uri.request_uri)
request['X-Atlas-Token'] = "#{atlas_api_token}"
request['Content-Type'] = 'application/json'
request.body = atlas_subnet_vars.to_json
http.request(request)
