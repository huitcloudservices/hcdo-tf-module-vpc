output "vpc_id" { value = "${aws_vpc.vpc.id}" }
output "vpc_cidr" { value = "${aws_vpc.vpc.cidr_block}" }
output "dhcp_option_set_id" { value = "${aws_vpc_dhcp_options.dhcp_option_set.id}" }
output "vpn_gateway_id" { value = "${aws_vpn_gateway.vpn_gateway.id}" }
output "vpc_route_table_id" { value = "${aws_route_table.vpc_route_table.id}" }
output "vpc_endpoint_id" { value = "${aws_vpc_endpoint.vpc_s3_endpoint.id}" }
output "vpc_endpoint_prefix_id" { value = "${aws_vpc_endpoint.vpc_s3_endpoint.prefix_list_id}" }

# Note these are comma delimited string primatives, you must split them on the calling end to get a list

output "subnet_elb_standard_ids" { value = "${join(",", aws_subnet.aws_vpc_elb_standard_subnets.*.id)}" }
output "subnet_elb_level4_ids" { value = "${join(",", aws_subnet.aws_vpc_elb_level4_subnets.*.id)}" }
output "subnet_app_standard_ids" { value = "${join(",", aws_subnet.aws_vpc_app_standard_subnets.*.id)}" }
output "subnet_app_level4_ids" { value = "${join(",", aws_subnet.aws_vpc_app_level4_subnets.*.id)}" }
output "subnet_db_standard_ids" { value = "${join(",", aws_subnet.aws_vpc_db_standard_subnets.*.id)}" }
output "subnet_db_level4_ids" { value = "${join(",", aws_subnet.aws_vpc_db_level4_subnets.*.id)}" }