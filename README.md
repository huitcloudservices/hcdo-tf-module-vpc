# AWS VPC Terraform Module

## Module Overview

This module sets up a VPC and all associated resources.  Before executing this module, you must follow the prerequisites below.

### Module Prerequisites

1. Create an environment in Atlas with all required variables except the subnet variables
2. Go to the [Jenkins Job](https://jenkins.cloudservices.huit.harvard.edu/view/Automation%20Jobs/job/HCDO_Terraform_VPC_Network_Generator/) and put in this name of the environment as well as the CIDR assigned to your VPC by ITS.  This CIDR must be a /20 at this time.  Any changes will be notated in the Jenkins job.
3. Queue/Execute the plan in Atlas using the usage below

### Module Resources Created
1. VPC & DHCP Options
2. VPC Route Table
3. VPC S3 Endpoint
4. VPC Subnets
5. VPC VPN/Virtual Gateway

## Module Invocation/Usage

```
module "vpc_mygroup_myenvironment" {
  source                = "bitbucket.org/huitcloudservices/hcdo-tf-module-vpc"
  vpc_cidr              = "10.0.0.0/16"
  group_name            = "mygroup"
  environment           = "dev"
  elb_standard_subnets  = "${split(",",var.elb_standard_subnets)}"
  elb_level4_subnets    = "${split(",",var.elb_level4_subnets)}"
  app_standard_subnets  = "${split(",",var.app_standard_subnets)}"
  app_level4_subnets    = "${split(",",var.app_level4_subnets)}"
  db_standard_subnets   = "${split(",",var.db_standard_subnets)}"
  db_level4_subnets     = "${split(",",var.db_level4_subnets)}"
}
```

## Module Parameters

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| vpc_cidr | CIDR Notated IP address network for VPC | - | yes |
| group_name | Group name for VPC (e.g. admints) | - | yes |
| environment | Environment for the application, one of: dev, test, stage, prod | - | yes |
| account_type | Account Type, one of: standard or level4. Level4 only for dedicated level4 accounts (e.g. peoplesoft) | `standard` | no |
| dns_name_servers | List of DNS servers to use, defaults to Harvard Infoblox failing back to Amazon local DNS | `["128.103.1.1", "128.103.200.101", "128.103.201.100", "AmazonProvidedDNS"]` | no |
| ntp_servers | List of NTP servers to use, defaults to Harvard time servers | `["128.103.1.6", "128.103.1.35", "128.103.38.165"]` | no |
| aws_region | Region string which the VPC resides in, defaults to us-east-1 | `us-east-1` | no |
| elb_standard_subnets | List of subnets for standard ELB's in CIDR notation | - | yes |
| elb_level4_subnets | List of subnets for Level4 ELB's in CIDR notation | - | yes |
| app_standard_subnets | List of subnets for Standard Apps in CIDR notation | - | yes |
| app_level4_subnets | List of subnets for Level4 Apps in CIDR notation | - | yes |
| db_standard_subnets | List of subnets for Standard DB's in CIDR notation | - | yes |
| db_level4_subnets | List of subnets for Level4 DB's in CIDR notation | - | yes |

## Module Outputs

| Name | Description |
|------|-------------|
| vpc_id | ID of the VPC |
| vpc_cidr | CIDR of the VPC |
| dhcp_option_set_id | ID of the DHCP Option Set  |
| vpn_gateway_id | ID of the VPN/VGW |
| vpc_route_table_id | ID of the Route Table (set as default) |
| vpc_endpoint_id | ID of the S3 Endpoint |
| vpc_endpoint_prefix_id | ID of the S3 Endpoint Prefixes |
| subnet_elb_standard_ids | Comma separated string of all ID's for ELB Standard Private Subnets |
| subnet_elb_level4_ids | Comma separated string of all ID's for ELB Level4 Private Subnets  |
| subnet_app_standard_ids | Comma separated string of all ID's for App Standard Private Subnets |
| subnet_app_level4_ids | Comma separated string of all ID's for App Level4 Private Subnets  |
| subnet_db_standard_ids | Comma separated string of all ID's for DB Standard Private Subnets |
| subnet_db_level4_ids | Comma separated string of all ID's for DB Level4 Private Subnets |
