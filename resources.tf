# Resources to create VPC

# Create VPC Resource
resource "aws_vpc" "vpc" {
  cidr_block            = "${var.vpc_cidr}"
  enable_dns_hostnames  = "true"
  enable_dns_support    = "true"

  tags {
    Name = "${lower(var.group_name)}-${lower(var.account_type)}-vpc"
    environment = "${lookup(var.environment_map, lower(var.environment))}"
    huit_assetid = "-1"
  }

}

# Create DHCP Resource and associate to VPC resource
resource "aws_vpc_dhcp_options" "dhcp_option_set" {
  domain_name_servers = ["${var.dns_name_servers}"]
  ntp_servers         = ["${var.ntp_servers}"]

  tags {
    Name          = "${lower(var.group_name)}-${lower(var.account_type)}-dhcpoptions"
    environment   = "${lookup(var.environment_map, lower(var.environment))}"
    huit_assetid  = "-1"
  }

}
resource "aws_vpc_dhcp_options_association" "dhcp_option_set_assoc" {
  dhcp_options_id = "${aws_vpc_dhcp_options.dhcp_option_set.id}"
  vpc_id          = "${aws_vpc.vpc.id}"
}

# Create VGW for VPC Direct Connect Attachment and auto-attach
resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name          = "${lower(var.group_name)}-${lower(var.account_type)}-vgw"
    environment   = "${lookup(var.environment_map, lower(var.environment))}"
    huit_assetid  = "-1"
  }

}

# Create Route Table for VPC and routes
# Note: Routes must be created as seperate resources,
# see notice at https://www.terraform.io/docs/providers/aws/r/route_table.html for more information
# Routes are propogated by the VPN Gateway and are not created in this module
resource "aws_route_table" "vpc_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"
  propagating_vgws = ["${aws_vpn_gateway.vpn_gateway.id}"]

  tags {
    Name          = "${lower(var.group_name)}-${lower(var.account_type)}-private-rt"
    environment   = "${lookup(var.environment_map, lower(var.environment))}"
    huit_assetid  = "-1"
  }

}

# Update Main Route table to new table
resource "aws_main_route_table_association" "main_route_table_assoc" {
  vpc_id = "${aws_vpc.vpc.id}"
  route_table_id = "${aws_route_table.vpc_route_table.id}"
}

# Create S3 Endpoint for VPC
resource "aws_vpc_endpoint" "vpc_s3_endpoint" {
  vpc_id = "${aws_vpc.vpc.id}"
  service_name = "com.amazonaws.${var.aws_region}.s3"
  route_table_ids = ["${aws_route_table.vpc_route_table.id}"]
}

# Create AWS Subnets - One each of standard/level4 for ELB, App, and DB

# Create AWS ELB Private Standard Subnets
resource "aws_subnet" "aws_vpc_elb_standard_subnets" {
  vpc_id = "${aws_vpc.vpc.id}"
  map_public_ip_on_launch = "false"
  count = "${var.number_of_azs}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${element(var.elb_standard_subnets, count.index)}"

  tags {
    Name = "${lower(var.group_name)}-standard-elb-private-${element(split("-", data.aws_availability_zones.available.names[count.index]),2)}-1"
  }

}

# Create AWS ELB Private Level4 Subnets
resource "aws_subnet" "aws_vpc_elb_level4_subnets" {
  vpc_id = "${aws_vpc.vpc.id}"
  map_public_ip_on_launch = "false"
  count = "${var.number_of_azs}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${element(var.elb_level4_subnets, count.index)}"

  tags {
    Name = "${lower(var.group_name)}-level4-elb-private-${element(split("-", data.aws_availability_zones.available.names[count.index]),2)}-1"
  }

}

# Create AWS App Private Standard Subnets
resource "aws_subnet" "aws_vpc_app_standard_subnets" {
  vpc_id = "${aws_vpc.vpc.id}"
  map_public_ip_on_launch = "false"
  count = "${var.number_of_azs}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${element(var.app_standard_subnets, count.index)}"

  tags {
    Name = "${lower(var.group_name)}-standard-app-private-${element(split("-", data.aws_availability_zones.available.names[count.index]),2)}-1"
  }

}

# Create AWS App Private Level4 Subnets
resource "aws_subnet" "aws_vpc_app_level4_subnets" {
  vpc_id = "${aws_vpc.vpc.id}"
  map_public_ip_on_launch = "false"
  count = "${var.number_of_azs}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${element(var.app_level4_subnets, count.index)}"

  tags {
    Name = "${lower(var.group_name)}-level4-app-private-${element(split("-", data.aws_availability_zones.available.names[count.index]),2)}-1"
  }

}

# Create AWS DB Private Standard Subnets
resource "aws_subnet" "aws_vpc_db_standard_subnets" {
  vpc_id = "${aws_vpc.vpc.id}"
  map_public_ip_on_launch = "false"
  count = "${var.number_of_azs}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${element(var.db_standard_subnets, count.index)}"

  tags {
    Name = "${lower(var.group_name)}-standard-db-private-${element(split("-", data.aws_availability_zones.available.names[count.index]),2)}-1"
  }

}

# Create AWS DB Private Level4 Subnets
resource "aws_subnet" "aws_vpc_db_level4_subnets" {
  vpc_id = "${aws_vpc.vpc.id}"
  map_public_ip_on_launch = "false"
  count = "${var.number_of_azs}"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block = "${element(var.db_level4_subnets, count.index)}"

  tags {
    Name = "${lower(var.group_name)}-level4-db-private-${element(split("-", data.aws_availability_zones.available.names[count.index]),2)}-1"
  }

}
